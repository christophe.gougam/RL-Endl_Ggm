package com.twitter.macron;

import com.twitter.macron.configuration.JpaConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages = {"com.twitter.macron"})
public class TweetServiceMain {

    public static void main(String[] args) {

        SpringApplication.run(TweetServiceMain.class, args);
    }
}
