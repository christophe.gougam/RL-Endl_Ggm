package com.twitter.macron.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.List;

import com.twitter.common.model.Tweet;
import com.twitter.macron.services.TweetService;

@Controller
public class TweetController {

    public static final Logger logger = LoggerFactory.getLogger(TweetController.class);

    @Autowired
    TweetService tweetService;

    // Recherche multi-critères, redirection vers index avec passage des paramètres de recherche entrés dans le formulaire
    @RequestMapping(value = "/results", method = RequestMethod.POST)
    public ModelAndView rechercheMultiCriteres(@RequestParam(required=false, value="dBas") String dBas, @RequestParam(required=false, value="dHaut") String dHaut, @RequestParam(required=false, value="sujet") String sujet) {
        ModelAndView mv = new ModelAndView("index");
        Timestamp tBas, tHaut;
        List<Tweet> tweetList;

        // Étant donné que dans le formaire on récupère un String, si il n'y a rien de tapé, la valeur sera vide et non null donc on crée un TImestamp soit avec la valeur null soit la valeur du String entrée
        if(dBas.equals("")) {
            tBas = null;
        } else {
            tBas = Timestamp.valueOf(dBas);
        }

        if(dHaut.equals("")) {
            tHaut = null;
        } else {
            tHaut = Timestamp.valueOf(dHaut);
        }

        tweetList = tweetService.findByMC(tBas, tHaut, sujet);
        mv.addObject(tweetList);

        if(tweetList.size() == 0) {
            mv.addObject("erreur", "Il n'y aucun tweet enregistré avec ces critères");
        } else {
            mv.addObject("info", "tweets trouvés");
        }

        return mv;
    }
}
