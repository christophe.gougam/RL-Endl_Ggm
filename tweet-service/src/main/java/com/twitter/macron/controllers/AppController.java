package com.twitter.macron.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppController {

    // On redirige l'URL racine de l'application vers l'authentification
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String index(Model model) {
        return ("authentification");
    }

}

