package com.twitter.macron.controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.twitter.macron.services.AuthentificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthentificationController {

    public static final Logger logger = LoggerFactory.getLogger(AuthentificationController.class);

    @Autowired
    AuthentificationService authentificationService;

    // Redirige vers la page d'index si le token est bon
    @RequestMapping(value = "/tweets", method = RequestMethod.POST)
    public ModelAndView searchMultipleOptions(@RequestParam(value="token") String token) {

        if (authentificationService.checkToken(token)) {
            return new ModelAndView("index");
        } else {
            ModelAndView mv = new ModelAndView("authentification");
            mv.addObject("erreur", "Token incorrect");
            return mv;
        }
    }

}
