package com.twitter.macron.services;

import com.twitter.common.model.Tweet;

import java.sql.Timestamp;
import java.util.List;

public interface TweetService {

    List<Tweet> findByMC(Timestamp timestampBas, Timestamp timestampHaut, String sujet);
}