package com.twitter.macron.services;

import org.springframework.stereotype.Service;

@Service
public class AuthentificationServiceImpl implements AuthentificationService {

    // On entre en dur le Token pour accéder à l'application, et on le compare avec le Token saisi
    @Override
    public boolean checkToken(String token) {
        return token.equals("macreille");
    }
}
