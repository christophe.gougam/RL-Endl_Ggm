package com.twitter.macron.services;

public interface AuthentificationService {

    boolean checkToken(String token);
}
