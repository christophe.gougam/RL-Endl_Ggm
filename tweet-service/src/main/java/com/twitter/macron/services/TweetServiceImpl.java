package com.twitter.macron.services;

import com.twitter.common.model.Tweet;
import com.twitter.macron.repositories.TweetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service("tweetService")
@Transactional
public class TweetServiceImpl implements TweetService {

    public static final Logger logger = LoggerFactory.getLogger(TweetServiceImpl.class);

    @Autowired
    private TweetRepository tweetRepository;

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Tweet> findByMC(Timestamp timestampBas, Timestamp timestampHaut, String sujet) {

        // On concatène une String Query en fonction des paramètres reçus
        String sQuery = "SELECT t FROM Tweet t";
        List<String> parametersToSet = new ArrayList<>();

        if (!sujet.equals("") || timestampBas != null || timestampHaut != null) {
            sQuery += " WHERE";
        }

        if (!sujet.equals("")) {
            sQuery += " t.content LIKE :content";
            parametersToSet.add("content");
        }
        if (timestampBas != null && timestampHaut != null) {
            if (parametersToSet.size() > 0) {
                sQuery += " AND";
            }
            sQuery += " t.date BETWEEN :timestampBas AND :timestampHaut";
            parametersToSet.add("timestampBas");
            parametersToSet.add("timestampHaut");
        }

        Query query = em.createQuery(sQuery);

        // On ajoute les paramètres à la Query
        for (String parameter : parametersToSet) {
            switch (parameter) {
                case "content":
                    query.setParameter("content", "%" + sujet + "%");
                    break;
                case "timestampBas":
                    query.setParameter("timestampBas", timestampBas);
                    break;
                case "timestampHaut":
                    query.setParameter("timestampHaut", timestampHaut);
                    break;
            }
        }

        logger.info("parametersToSet : " + parametersToSet.toString());

        return query.getResultList();
    }
}
