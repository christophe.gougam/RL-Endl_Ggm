<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Connexion</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <body style="display: flex; ">
        <form action='/twitter/tweets' autocomplete="off"  method='post' style="margin: auto; background-image: linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%); padding: 50px; border-radius: 10px;">
            <c:if test="${!empty erreur}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                    <c:out value='${erreur}' />
                </div>
            </c:if>
            <div class="form-group">
                <label for="token">Token</label>
                <input type="password" class="form-control" id="token" name="token" autofocus>
                <small id="ave" class="form-text text-muted">Ave Macron.</small><br/><br/>
                <button type="submit" class="btn btn-primary">Connexion</button>
            </div>
        </form>
    </body>
</html>
