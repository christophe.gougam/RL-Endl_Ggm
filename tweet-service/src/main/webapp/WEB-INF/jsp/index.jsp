<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Tweets</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <form action='/twitter/results' autocomplete="off" method='post' style="padding: 0;" class="panel panel-default">
            <div class="panel-heading">
                <h3>Recherche multi-critères</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-info alert" role="alert">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    Vous pouvez les dates et/ou le sujet.
                </div>
                <div class="form-group">
                    <label >Date début période :</label>
                    <input type="text" name='dBas' class="form-control" placeholder="aaaa-mm-jj hh:mm:ss">
                </div>

                <div class="form-group">
                    <label >Date fin période :</label>
                    <input type="text" name='dHaut' class="form-control" placeholder="aaaa-mm-jj hh:mm:ss">
                </div>

                <div class="form-group">
                    <label >Sujet :</label>
                    <input type="text" name='sujet' class="form-control" placeholder="Chocolatine, France, Poutine, Kaaris">
                </div>

                <button type="submit" class="btn btn-warning" >Rechercher</button>
            </div>
        </form>

        <div style="padding: 0 20px;">
            <c:choose>
                <c:when test="${!empty info}">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
                        <c:out value='${tweetList.size()} ${info}' />
                    </div>
                </c:when>
                <c:when test="${!empty erreur}">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                        <c:out value='${erreur}' />
                    </div>
                </c:when>
            </c:choose>

            <!-- Si il n'y a pas de tweets retournés (donc soit pas de recherche soit pas de résultat) on afiche pas cette partie -->
            <c:if test="${tweetList != null}">
                <c:forEach items="${tweetList}" var="tweet">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left"><button class="btn btn-default disabled"><span class="glyphicon glyphicon-barcode"></span>&nbsp;${tweet.id}</button>&nbsp;<button class="btn btn-warning disabled">${tweet.username}</button></div>
                            <div class="pull-right">
                                <p class="btn btn-primary disabled">Retweets : ${tweet.retweets}</p>
                                <p class="btn btn-info disabled">Likes : ${tweet.likes}</p>
                                <p class="btn btn-success disabled">${tweet.date}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <p>${tweet.content}</p>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </body>
</html>
