package com.twitter.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "tweets")
public class Tweet implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    // Taille de 1000 au cas où étant donné qu'un tweet fait 240 caractères + les pseudos, liens...
    @Column(name = "content", columnDefinition="VARCHAR(1000)")
    private String content;

    @Column(name = "date")
    private Timestamp date;

    @Column(name = "retweets")
    private Integer retweets;

    @Column(name = "likes")
    private Integer likes;

    public Tweet() {}

    public Tweet(Long id, String username, String content, Timestamp date, int retweets, int likes) {
        this.id = id;
        this.username = username;
        this.content = content;
        this.date = date;
        this.retweets = retweets;
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "username='" + username + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                ", retweets=" + retweets +
                ", likes=" + likes +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getRetweets() {
        return retweets;
    }

    public void setRetweets(int retweets) {
        this.retweets = retweets;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
