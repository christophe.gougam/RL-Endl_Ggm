package com.twitter.scraper;

import com.twitter.scraper.configuration.JpaConfiguration;
import com.twitter.scraper.service.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages = {"com.twitter.scraper"})
public class TweetScraper extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private TweetService tweetService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder tweetScraper) {
        return tweetScraper.sources(TweetScraper.class);
    }

    public static void main(String[] args) {

        SpringApplication.run(TweetScraper.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        tweetService.saveTweets();
    }
}