package com.twitter.scraper.service;

import com.twitter.common.model.Tweet;
import com.twitter.scraper.repositories.TweetRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import twitter4j.*;

import java.sql.Timestamp;

@Service("tweetService")
@Transactional
public class TweetServiceImpl implements TweetService {

    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(TweetServiceImpl.class);

    @Autowired
    private TweetRepository tweetRepository;

    private TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

    @Override
    public void saveTweets() {
        FilterQuery tweetFilterQuery = new FilterQuery();
        // On ajoute un filtre pour récupérer uniquement les tweets en français
        tweetFilterQuery.language(new String[]{"fr"});
        // ON ajoute un autre filtre pour récupérer uniquement les tweets postés entre ces coordonnées (Bounding Box de la France)
        tweetFilterQuery.locations(new double[][]{{-54.5247541978, 2.05338918702}, {9.56001631027, 51.1485061713}});
        twitterStream.addListener(new StatusListener() {

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int i) {

            }

            @Override
            public void onScrubGeo(long l, long l1) {

            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {

            }

            @Override
            public void onException(Exception e) {

            }

            @Override
            @Transactional
            public void onStatus(Status status) {
                Tweet t = new Tweet();
                t.setUsername(status.getUser().getScreenName());
                t.setDate(new Timestamp(status.getCreatedAt().getTime()));
                t.setContent(status.getText());
                t.setLikes(status.getFavoriteCount());
                t.setRetweets(status.getRetweetCount());
                tweetRepository.save(t);
            }
        });

        twitterStream.filter(tweetFilterQuery);
    }

}
